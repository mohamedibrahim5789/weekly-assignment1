package com.greatLearning.assignment;


	class SuperDepartment {
		
		//declare method departmentName of return type string
		public String departmentName() {
			return "Super Department";
		}
		
		//declare method getTodaysWork of return type string
		public String getTodaysWork() {
			return "No work as of now";
		}
		
		//declare method getWorkDeadline of return type string
		public String getWorkDeadline() {
			return "Nil";
		}
		
		//declare method isTodayAHoliday of type string
		public String isTodayAHoliday() {
			return "Today is not a holiday";
		}
	}

	class AdminDepartment extends SuperDepartment{
		
		//declare method departmentName of return type string
		public String departmentName() {
			return "Admin Department";
		}
		
		//declare method getTodaysWork of return type string
		public String getTodaysWork() {
			return "Complete your documents Submission";
					
		}
		
		//declare method getWorkDeadline of return type string
		public String getWorkDeadline() {
			return "Complete by EOD";
		}
		
	}

	class HrDepartment extends SuperDepartment{
		
		//declare method departmentName of return type string
		public String departmentName() {
			return "HR Department";
		}
		
		//declare method getTodaysWork of return type string
		public String getTodaysWork() {
			return "Fill todays worksheet and mark your attendance";
		}
		
		//declare method getWorkDeadline of return type string
		public String getWorkDeadline() {
			return "Complete by EOD";
		}
		
		//declare method doActivity of return type string
		public String doActivity() {
			return "Team Lunch";
		}
	}

	class TechDepartment extends SuperDepartment{
			
		//declare method departmentName of return type string
		public String departmentName() {
			return "Tech Department";
		}
		
		//declare method getTodaysWork of return type string
		public String getTodayWork() {
			return "Complete coding of module 1";
		}
		
		//declare method getTechStackInformation of return type string
		public String getTechStackInformation() {
			return "core Java";
		}
		
		//declare method getWorkDeadline of return type string
		public String getWorkDeadline() {
			return "Complete by EOD";
		}
		
	}
public class abc {
	

		public static void main(String[] args) {

			// create the object of TechDepartment and use all the methods
			SuperDepartment s=new SuperDepartment();
			AdminDepartment ad=new AdminDepartment();
			System.out.println(ad.departmentName());
			System.out.println(ad.getTodaysWork());
			System.out.println(ad.getWorkDeadline());
			System.out.println(s.isTodayAHoliday());
			System.out.println();
			
			// create the object of Hr Department and use all the methods 
			HrDepartment hd=new HrDepartment();
			System.out.println(hd.departmentName());
			System.out.println(hd.getTodaysWork());
			System.out.println(hd.getWorkDeadline());
			System.out.println(s.isTodayAHoliday());
			System.out.println(hd.doActivity());
			System.out.println();
			
			// create the object of TechDepartment and use all the methods
			TechDepartment td=new TechDepartment();
			System.out.println(td.departmentName());
			System.out.println(td.getTodaysWork());
			System.out.println(td.getWorkDeadline());
			System.out.println(s.isTodayAHoliday());
			System.out.println(td.getTechStackInformation());
			System.out.println();
			
		
		}
		

	}
